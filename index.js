// loops 

// while loop

let count = 5; // number of times how we repeat the code

while (count > 0) {
	console.log('Sylvan');
	count--; //
}

let x = 1;

while (x <=5) {
	console.log(x);
	x++;
}


let fruits = ['Banana', 'Mango'];
	y = 0;

while (y < 2) {
	console.log(fruits[y]);
	y++;
}

let mobiles = ['Samsung Galaxy S21', 'Iphone 13', 'Xiaomi 11T', 'RealMe C', 'huawei Nova 8', 'Pixel 5', 'Asus ROG 6', 'Nokia', 'Cherry Mobile'];

	n = 0;

while (n <= mobiles.length-1) {
	console.log(mobiles[n]);
	n++;
}


// do-while = do the statement once, before running the condition.

let w = 1;

do {
	console.log('Wendy');
	w++;
} while(w <= 6);

let j = 6;

do{
	console.log(`Do-While count ${j}`);
	j--;
} while (j==7);

//versus
console.log('==========Youn=========');

while (j==7){
	console.log(`Do-While count ${j}`);
	j--;
}
console.log('=======mini activity====');

let i = 0;
	compBrands = ['Apple Macbook Pro', 'HP Notebook', 'Asus', 'Lenovo', 'Acer', 'dell', 'huawei'];

do {
	console.log(compBrands[i]);
	i++;

} while (i <= compBrands.length-1);

// for loop



for (let i=0; i <=5; i++) {
	console.log(i);
}

console.log('===== mini activity ====');

let colors = ['red', 'green', 'blue', 'yellow', 'purple', 'white', 'black'];

for (let i=0; i <= colors.length-1; i++){
	console.log(colors[i]);
}

let ages = [18, 19, 20, 21, 24, 25];
for (i=0; i <= ages.length-1; i++){
	if(ages[i] == 21 || ages[i] == 18) {
		continue;
	}
		console.log(ages[i]);
}

let student = ['den', 'Jayson', 'Marvin', 'Rommel'];

for (let i=0; i<=student.length-1; i++){
	if (student[i] == 'Jayson'){
	 console.log(student[i]);
		break;
	}
}


console.log('==== Coding Challenge #1 ====');

let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

for(i=0; i<= adultAge.length-1; i++) {
	if( adultAge[i] <=19) {
		continue;
	} console.log(adultAge[i]);
}

console.log('===== coding Challenge #2 ====');

let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
	for(i=0; i <= students.length-1; i++) {
		if (students[i] == studentName) {
			console.log(students[i]);
			break;
		}  
	}
}

searchStudent('Jazz');